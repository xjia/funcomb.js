funcomb.js
=============

Functional combinators for JavaScript

Installation
------------

```html
<script src="funcomb.js"></script>
```

Usage
-----

The following code snippet calculates the 33rd Fibonacci number recursively.
This implementation runs in exponential time so it's pretty slow.

```js
recursive(function(n) {
  return n <= 1 ? 1 : this(n-1) + this(n-2);
})(33)
```

The following code snippet runs in linear time (fast as you expected) using memoization.

```js
recursive(memoized(function(n) {
  return n <= 1 ? 1 : this(n-1) + this(n-2);
}))(33)
```

The following code snippet calculates the LCS of two strings `X` and `Y`.
`sortBy` and `last` are functions provided by Sugar.js.

```js
function pairwise_lcs(X, Y) {
  return recursive(memoized(function(i, j) {
    if (i < 0 || j < 0)
      return "";
    else if (X[i] === Y[j])
      return this(i-1, j-1) + X[i];
    else
      return [this(i, j-1), this(i-1, j)].sortBy("length").last();
  }))(X.length-1, Y.length-1);
}
```

The following code snippet calculates the sum of 1 and 2 in a fancy way.

```js
currify(function(a,b){ return a+b; }, 1)(2)
```

The following code snippet shows the composition functionality.

```js
composition(console.log, function(x){ return x+1; })(100);
```

The following code snippet shows the identity function.

```js
console.log(identity(123)());
```
