function memoized(f) {
  var memo = {};
  return function() {
    var key = JSON.stringify(arguments);
    if (memo[key] === undefined)
      memo[key] = f.apply(this, arguments);
    return memo[key];
  };
}

function recursive(f) {
  var recursive_f = function() {
    return f.apply(recursive_f, arguments);
  };
  return recursive_f;
}

function currify(f) {
  var args = Array.prototype.slice.call(arguments);
  args.shift();
  return function() {
    return f.apply(this, args.concat(Array.prototype.slice.call(arguments)));
  };
}

function composition() {
  var f = arguments;
  return function() {
    var i, r = arguments;
    for (i = f.length - 1; i >= 0; i--) {
      if (f[i] === console.log
          || f[i] === console.error
          || f[i] === console.warn
          || f[i] === console.info)
        r = [ f[i].apply(console, r) ];
      else
        r = [ f[i].apply(this, r) ];
    }
    return r;
  };
}

function identity(i) {
  return function(){ return JSON.parse(JSON.stringify(i)); };
}
